"""Stream type classes for tap-auth0."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_auth0.client import auth0Stream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class UsersStream(auth0Stream):
    """Define custom stream."""
    name = "users"
    path = "/users"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property(
            "created_at",
            th.DateTimeType,
            description="User created at"
        ),
        th.Property(
            "email",
            th.StringType,
            description="The user's email address"
        ),
        th.Property(
            "email_verified",
            th.BooleanType,
            description="The user's email address has been verified or not."
        ),
        th.Property("identities",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("user_id", th.StringType),
                            th.Property("provider", th.StringType),
                            th.Property("connection", th.StringType),
                            th.Property("isSocial", th.BooleanType),
                        )
                    )
                    ),
        th.Property("last_password_reset", th.DateTimeType),
        th.Property("nickname", th.StringType),
        th.Property("picture", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("user_id", th.StringType),
    ).to_dict()
