"""auth0 tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_auth0.streams import (
    auth0Stream,
    UsersStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    UsersStream,
]


class Tapauth0(Tap):
    """auth0 tap class."""
    name = "tap-auth0"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "non_interactive_client_id",
            th.StringType,
            required=True,
            description="The non_interactive_client_id to authenticate against the API service"
        ),
        th.Property(
            "non_interactive_client_secret",
            th.StringType,
            required=True,
            description="The non_interactive_client_secret to authenticate against the API service"
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.mysample.com",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == '__main__':
    Tapauth0.cli()
